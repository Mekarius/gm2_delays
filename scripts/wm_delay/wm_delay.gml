function Delay(_originalCallback, _timer, _storingData=noone, autoStart = 1) constructor
{ 
	if !variable_global_exists("__DelayDefaultUnit")
	{
		global.__DelayDefaultUnit = time_source_units_frames;
		global.__IntervalDefaultUnit = time_source_units_frames;
		global.__DelayIntervalDefaultSource = time_source_game;
	}

	if instanceof(self) == "Interval" || instanceof(self) == "Interval_S"
		intervalOther = other;
	else if instanceof(self) == "Delay" || instanceof(self) == "Delay_S"
		delayOther = other; 
		
	data = _storingData;
	timer = _timer;
	repetition = 1;
	iteration = 0;
	originalCallback = _originalCallback;
	callerID = method_get_self(_originalCallback);  
	
	// Fake callback send to time source, that will call the real callback, but with right scope
	callback = function(){
		var _self = self;
		var callback = originalCallback;
		
		var _other;
		if instanceof(self) == "Interval"  || instanceof(self) == "Interval_S"
			_other = intervalOther;
		else if instanceof(self) == "Delay"  || instanceof(self) == "Delay_S"
			_other = delayOther;
		
		var originalCallerID = callerID;
		
		iteration++;
		with _other
		{
			with originalCallerID callback(_self);
		}
	} 
	
	timeSourceID = time_source_create(global.__DelayIntervalDefaultSource, _timer, global.__DelayDefaultUnit, callback, [], 1, time_source_expire_after);
	
	static start = function(){
		if time_source_exists(timeSourceID) && time_source_get_state(timeSourceID) == time_source_state_initial
			time_source_start(timeSourceID);
	}
	
	static play = function(){ // Alias
		start();
	}
	
	static stop = function(){
		if time_source_exists(timeSourceID) && (time_source_get_state(timeSourceID) == time_source_state_active || time_source_get_state(timeSourceID) == time_source_state_paused)
			time_source_stop(timeSourceID);
	}
	
	static resume = function(){
		if time_source_exists(timeSourceID) && time_source_get_state(timeSourceID) == time_source_state_paused
			time_source_start(timeSourceID);
	}
	
	static pause = function(){
		if time_source_exists(timeSourceID) && time_source_get_state(timeSourceID) == time_source_state_active
			time_source_pause(timeSourceID);
	}
	
	static destroy = function(){
		if time_source_exists(timeSourceID)
			time_source_destroy(timeSourceID);
	} 
	
	static clear = function(){ // Alias of destroy basically
		destroy();	
	}
	
	static getRemaining = function(){
		if time_source_exists(timeSourceID)
			return time_source_get_time_remaining(timeSourceID);
		return 0;
	}
	 
	static getRemainingTotal = function(){
		if iteration == -1
			return infinity;
			
		var spend = iteration * timer + ( timer - getRemaining());
		var total = repetition * timer;
		
		return total - spend;
	}
	 
	static getElapsed = function(){
		return timer - getRemaining();
	}
	
	static getElapsedTotal = function(){
		return iteration * timer + getElapsed();
	} 
	
	static getID = function(){
		return timeSourceID;	
	}
	
	static changePeriod = function(newPeriod){ // Resets it
		if time_source_exists(timeSourceID)
		{
			iteration = 0;
			timer = newPeriod;
			time_source_reconfigure(timeSourceID, 
				newPeriod,
				time_source_get_units(timeSourceID),
				callback,
				[],
				repetition,
				time_source_expire_after
			);	
			start(); // Start back
		}
	}
	
	static changeUnits = function(newUnits){  // Resets it
		if time_source_exists(timeSourceID)
		{
			iteration = 0;
			time_source_reconfigure(timeSourceID, 
				time_source_get_period(timeSourceID),
				newUnits,
				callback,
				[],
				repetition,
				time_source_expire_after
			);	
			start(); // Start back
		}
	}
	
	
	static changeRepetition = function(newRepetition){  // Resets it
		if time_source_exists(timeSourceID)
		{
			iteration = 0;
			repetition = newRepetition;
			time_source_reconfigure(timeSourceID, 
				time_source_get_period(timeSourceID),
				time_source_get_units(timeSourceID),
				callback,
				[],
				newRepetition,
				time_source_expire_after
			);	 
			start(); // Start back
		}
	}
	
	if autoStart == 1
		start(); 
}

function Interval(_originalCallback, _timer, _repetition = -1, _storingData, autoStart) : Delay(_originalCallback, _timer, _storingData, autoStart = 0) constructor
{
	repetition = _repetition;
	time_source_reconfigure(timeSourceID, _timer, global.__IntervalDefaultUnit, callback, [], repetition, time_source_expire_after);
	if autoStart == 1 
		start();
}

function Delay_S(_originalCallback, _timer, _storingData, autoStart) : Delay(_originalCallback, _timer, _storingData, autoStart) constructor
{
	changeUnits(time_source_units_seconds);
	if autoStart == 1
		start();
}

function Interval_S(_originalCallback, _timer, _repetition, _storingData, autoStart) : Interval(_originalCallback, _timer, _repetition, _storingData, autoStart) constructor
{
	changeUnits(time_source_units_seconds);
	if autoStart == 1
		start();
}