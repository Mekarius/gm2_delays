/*
# Examples 
	## Example 1 : Creating a new delay 
	## Example 2 : Creating a new interval
	## Example 3 : Creating a new delay in seconds
# Advanced Examples
	## Example 4 : Pausing/Resuming & Transfer data in the callback
*/

// Set demoExample to 1-4 to execute one of the examples
var demoExample = 0; // No example executed by default 

if demoExample == 1
{ 
    var myDelay = new Delay(function(myDelay) {
        show_debug_message("This will be shown in 100 frames");
    }, 100);
    /* 
    Output :  
        100 frames : This will be shown every 100 frames
    */
}
 
if demoExample == 2
{ 
    var myInterval = new Interval(function(myInterval) {
        show_debug_message("This will be shown every 50 frames");
    }, 50);
    /* 
    Output : 
         50 frames : This will be shown every 50 frames
        100 frames : This will be shown every 50 frames
        150 frames : This will be shown every 50 frames
        ...
    */
}

if demoExample == 3
{ 
	var myDelayInSeconds = new Delay_S(function(){
		show_debug_message("This will be shown in 3 seconds");
	}, 3);
    /*
    Output :
        3000ms : This will be shown in 3 seconds
    */
}

if demoExample == 4
{ 
   
    var firstDelay = new Delay(function(firstDelay) {
        show_debug_message("This would be shown after 50 frames if no pause occures, but we paused for (100-10) 90 frames, so it's 50+90 (140) frames later."); 
    }, 50);

    var pauseDelay10 = new Delay(function(pauseDelay10) { // We'll pause the first delay after 10 frames
        var firstDelay = pauseDelay10.data; // 3rd argument of Delay/Interval is stored in .data so you can retrieve it
		
        show_debug_message("Pausing the first delay");
        show_debug_message("Elapsed frames : " + string(firstDelay.getElapsed()));
        show_debug_message("Remaining frames : " + string(firstDelay.getRemaining()));
		
        firstDelay.pause();
    }, 10, firstDelay);
 
    var resumeDelay100 = new Delay(function(resumeDelay100) { // We'll resume the first delay after 100 frames
	    var firstDelay = resumeDelay100.data; // 3rd argument of Delay/Interval is stored in .data so you can retrieve it
        show_debug_message("Resuming the first delay");
	    firstDelay.resume();
    }, 100, firstDelay);  
	
	/*
	Output :
		10 frames :	Pausing the first delay
		10 frames :	Elapsed frames : 10
		10 frames :	Remaining frames : 40
		100 frames :	Resuming the first delay
		140 frames :	This would be shown after 50 frames if no pause occure, but we paused for 90 frames, so it's 50+90 (140) frames later.
	*/ 
}

if demoExample == 0
	show_message("No Delay demo executed, check wm_delay_examples to execute a demo, and checkout Ouput for demo execution.");