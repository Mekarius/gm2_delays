# GM2_Delays
2 objects to handle new GM2 timers a little more easily. + 2 variants for seconds
Default usage is per frame, variants for seconds exists with _S;

## How to create the new Objects
```js
    var myDelay = new Delay(...); // Delay in frames
    var myInterval = new Interval(...); // Interval in frames

    var myDelayInSeconds = new Delay_S(...); // Delay in seconds
    var myIntervalInSeconds = new Interval_S(...); // Interval in seconds
```

## Arguments list for Delay
```js
    var myDelay = new Delay(
        callback,
        timerInFrames,
        [additionalData = undefined], // This will be accessible with myDelay.data
        [autoStart = 1] // The delay will start immediately after creation, 1 by default, 0 to wait for play() or start()
    );
```
## Arguments list for Interval
```js
    var myInterval = new Interval(
        callback,
        timerInFrames,
        [additionalData = undefined], // This will be accessible with myInterval.data
        [autoStart = 1], // The interval will start immediately after creation, 1 by default, 0 to wait for play() or start()
        [repetition =  -1] // -1 : Will continue for ever, 1+ : Number of repetition you want 
    );
```

# Examples 
## Example 1 : Creating a new delay 
```js
    var myDelay = new Delay(function(myDelay){
        show_debug_message("This will be shown in 100 frames");
    }, 100)
```
```js
    /*
    Output :
        100 frames : This will be shown in 100 frames
    */
```

## Example 2 : Creating a new interval
```js
    var myInterval = new Interval(function(myInterval){
        show_debug_message("This will be shown every 100 frames");
    }, 100)
```
```js
    /*
    Output :
        100 frames : This will be shown every 100 frames
        200 frames : This will be shown every 100 frames
        300 frames : This will be shown every 100 frames...
    */
```

## Example 3 : Creating a new delay in seconds
```js
    var myDelayInSeconds = new Delay_S(function(myDelay){
        show_debug_message("This will be shown in 3 seconds");
    }, 3)
```
```js
    /*
    Output :
        3000ms : This will be shown in 3 seconds
    */
```

# Advanced Examples
## Example 4 : Pausing/Resuming & Transfer data in the callback
```js 
    var firstDelay = new Delay(function(firstDelay) {
        show_debug_message("This would be shown after 50 frames if no pause occures, but we paused for (100-10) 90 frames, so it's 50+90 (140) frames later."); 
    }, 50);

    var pauseDelay10 = new Delay(function(pauseDelay10) { // We'll pause the first delay after 10 frames
        var firstDelay = pauseDelay10.data; // 3rd argument of Delay/Interval is stored in .data so you can retrieve it
		
        show_debug_message("Pausing the first delay");
        show_debug_message("Elapsed frames : " + string(firstDelay.getElapsed()));
        show_debug_message("Remaining frames : " + string(firstDelay.getRemaining()));
		
        firstDelay.pause();
    }, 10, firstDelay);
 
    var resumeDelay100 = new Delay(function(resumeDelay100) { // We'll resume the first delay after 100 frames
	    var firstDelay = resumeDelay100.data; // 3rd argument of Delay/Interval is stored in .data so you can retrieve it
        show_debug_message("Resuming the first delay");
	    firstDelay.resume();
    }, 100, firstDelay);  
	
	/*
	Output :
		10 frames :	Pausing the first delay
		10 frames :	Elapsed frames : 10
		10 frames :	Remaining frames : 40
		100 frames :	Resuming the first delay
		140 frames :	This would be shown after 50 frames if no pause occure, but we paused for 90 frames, so it's 50+90 (140) frames later.
	*/
	
```

# All Methods & Attributes
```js
    var myDelay = new Delay( function(){ /* Nothing */ }, 100); // Delay that does nothing in 100 frames
```

## Methods that affect the timer
- **myDelay.start()**   : Start the timer system, by default autostart=1 will automatically start any delay/interval
    - _**myDelay.play()**_    : _Alias of start_
- **myDelay.stop()**    : Stop the timer system
- **myDelay.pause()**   : Pause the timer and can be resumed
- **myDelay.resume()**   : Resume a timer that has been paused
- **myDelay.destroy()**    : Destroy Stop the timer system
    - _**myDelay.clear()**_   : _Alias of destroy_

## Methods to get information
- **myDelay.getElapsed()** : Returns the elapsed period since start for delay, and since last iteration for interval
- **myDelay.getRemaining()** : Returns the remaining period left until the callback is executed
- **myDelay.getElapsedTotal()** : _Useful for Interval only;_ Returns the total elapsed period since start
- **myDelay.getRemainingTotal()** : _Useful for interval only;_ Returns the total remaining time until the interval is fully done
- **myDelay.getID()** : Returns the original timer_source_id

## Methods to change how the current timer is working
**Be careful, using those methods resets totally the timer when called, and start it back right away.**
- **myDelay.changePeriod(newPeriod)** : Change the period/timer for callback execution
- **myDelay.changeUnits(newUnits)** : Change units for the period ( Use **time_source_units_frames** for frames, **time_source_units_seconds** for seconds. )
- **myDelay.changeRepetition(newRepetition)** : Change the number of repetition, 1 by default for Delay, -1 (Infinite) by default for Interval

## Attributes you can read from
- **myDelay.data** : Data you pass as 3rd parameter that will be accessible in the scope of the callback by using data from first argument ( argument[0].data ) 
- **myDelay.timer** : Period for that delay/interval
- **myInterval.iteration**: Current iteration of the interval
- **myInterval.repetition** : Number of repetition of the interval
- **myInterval.callerID** : Instance ID ( if it exists ) that created the callback
- **myInterval.delayOther** : "other" that was available at the line the interval was created. 
    - _Exists only with Delay/Delay_S_
- **myInterval.intervalOther** : "other" that was available at the line the interval was created. 
    - _Exists only with Interval/Interval_S_
 


# Globals default variables that are editable in wm_delay.gml line 5-7
```js
    // Units
    global.__DelayDefaultUnit = time_source_units_frames;
    global.__IntervalDefaultUnit = time_source_units_frames;
    // Values for units should be either :
    //   - time_source_units_frames : for a period per frame - Default
    //   - time_source_units_second : for a period per second


    // Source of time
    global.__DelayIntervalDefaultSource = time_source_game; 
    // Values for source of time should be either :
    //   - time_source_game : For it to be dependent of game itself
    //   - time_source_global : To not be dependent of game, if using this setting, methods like pauses etc... cannot be used.
```




